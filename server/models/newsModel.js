const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const news = new Schema ({
    titulo: {type: String},
    descripcion: {type: String},
    link: {type: String},
    fecha: {type: String},
    id_fuente_noticia: {type: String},
    id_usuario: {type: String}, 
    id_categoria: {type: String}
});

module.exports = mongoose.model("news", news);