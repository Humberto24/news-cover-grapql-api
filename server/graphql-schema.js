const { buildSchema } = require('graphql');
exports.graphQLschema = buildSchema(`
  type Query {
    news(id_usuario: String): [News]
    buscarNoticias(id_usuario: String, titulo: String): [News]
    buscarNoticiasPorEtiqueta(id_usuario: String, etiqueta: String): [News]
  }

  type Mutation {
    createNews(titulo: String!, descripcion: String!, link: String!, fecha: String!, id_fuente_noticia: String!,
       id_usuario: String!, id_categoria: String! ): News
  }

  type News {
    id: ID!
    titulo: String!
    descripcion: String!
    link: String!
    fecha: String!
    id_fuente_noticia: String!
    id_usuario: String!
    id_categoria: String!
    etiqueta: [String!]
  }`);