const express = require('express');
const graphqlHTTP = require('express-graphql');
const { graphQLschema } = require('./graphql-schema.js')
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/proyecto2-api");

const News = require("./models/newsModel");

// functions


/**
 * Metodo para obtener las noticias
 * @param {*} req 
 * @returns 
 */
const getNews = async (req) => {
  return News.find({"id_usuario": req.id_usuario},function (err, news) {
    if (err) {
      return "There was an error"
    }
    return news;
  })
};

/**
 * Metodo para buscar noticias
 * @param {*} req 
 * @returns 
 */
const buscarNoticias = async (req) => {
  return News.find({"id_usuario": req.id_usuario, "titulo": new RegExp(req.titulo, "i")}, function (err, news) {
    if (err) {
      return "There was an error"
    }
    return news;
  })
};

/**
 * Metodo para buscar las noticias por una etiqueta
 * @param {*} req 
 * @returns 
 */
const buscarNoticiasPorEtiqueta = async (req) => {
  return News.find({"id_usuario": req.id_usuario, "etiqueta": {$in: [req.etiqueta]}}, function (err, news) {
    if (err) {
      return "There was an error"
    }
    return news;
  })
};

// expose in the root element the different entry points of the
// graphQL service
const root = {
  //Queries
  news: (req) => getNews(req),
  buscarNoticias: (req) => buscarNoticias(req),
  buscarNoticiasPorEtiqueta: (req) => buscarNoticiasPorEtiqueta(req),

};

// instance the expressJS app
const app = express();
// check for cors
const cors = require("cors");
app.use(cors({
  domains: '*',
  methods: "*"
}));

//one single endpoint different than REST
app.use('/graphql', graphqlHTTP({
  schema: graphQLschema,
  rootValue: root,
  graphiql: true, 
}));

app.listen(4000, () => console.log('Now browse to localhost:4000/graphql'));